import { Navbar, Nav } from "react-bootstrap";
import { Fragment, useContext } from "react";
import UserContext from "../UserContext";
import { NavLink } from "react-router-dom";

export default function AppNavbar() {
	const { user } = useContext(UserContext);
	let leftNav =
		user.email != null ? (
			<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
		) : (
			<Fragment>
				<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
				<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
			</Fragment>
		);

	return (
		<Navbar bg="primary" expand="lg" className="px-4">
			<Navbar.Brand href="/">Course Booking</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="me-auto">
					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
				</Nav>
				<Nav>{leftNav}</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
}
