import { useEffect, useState } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";

export default function CourseCard(props) {
	const [enrollees, setEnrollees] = useState(0);
	const [seats, setSeats] = useState(10);
	const [isDisabled, setIsDisabled] = useState(false);

	function addEnrollee() {
		if (seats > 0) {
			setEnrollees(enrollees + 1);
			setSeats(seats - 1);
		} else if (seats <= 0) {
			alert("No more seats");
		}
	}

	useEffect(() => {
		if (seats <= 0) {
			setIsDisabled(true);
		}
	}, [seats]);

	return (
		<Container fluid className="my-3">
			<Row className="justify-content-center">
				<Col className="my-2">
					<Card className="p-2">
						<Card.Title>{props.title}</Card.Title>
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{props.description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>{props.price}</Card.Text>
						<Card.Text>Enrollees: {enrollees}</Card.Text>
						<Card.Text>Seats: {seats}</Card.Text>
						<Button
							style={{ width: "8rem" }}
							onClick={addEnrollee}
							variant="primary"
							disabled={isDisabled}
						>
							Enroll
						</Button>
					</Card>
				</Col>
			</Row>
		</Container>
	);
}
