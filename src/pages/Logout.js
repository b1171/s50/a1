import { useContext, useEffect } from "react";
import { Redirect } from "react-router-dom";
import UserContext from "../UserContext";

export default function Logout() {
	const { setUser, unsetUser } = useContext(UserContext);

	unsetUser();

	useEffect(() => {
		setUser({ email: null });
	}, []);

	return <Redirect to="/login" />;
}
