import { useState } from "react";
import { Container } from "react-bootstrap";
import { Route, Switch } from "react-router-dom";
import { BrowserRouter } from "react-router-dom/cjs/react-router-dom.min";
import "bootstrap/dist/css/bootstrap.min.css";
import AppNavbar from "./components/AppNavbar";
import Courses from "./pages/Courses";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import { UserProvider } from "./UserContext";

export default function App() {
	// React context is nothing but a global state to the app.
	// Context helps you broadcast the data and changes to the data

	const HOST = "https://s32-36-booking-system.herokuapp.com";
	const [user, setUser] = useState({
		email: localStorage.getItem("email"),
	});

	const unsetUser = () => {
		localStorage.clear();
	};

	return (
		<UserProvider value={{ HOST, user, setUser, unsetUser }}>
			<BrowserRouter>
				<AppNavbar />
				<Container fluid>
					<Switch>
						<Route exact path="/" component={Home} />
						<Route exact path="/courses" component={Courses} />
						<Route exact path="/register" component={Register} />
						<Route exact path="/login" component={Login} />
						<Route exact path="/logout" component={Logout} />
						<Route exact path="*" component={Error} />
					</Switch>
				</Container>
			</BrowserRouter>
		</UserProvider>
	);
}
